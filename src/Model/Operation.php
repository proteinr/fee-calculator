<?php

declare(strict_types=1);

namespace App\Model;

use App\Enum\ClientType;
use App\Enum\Currency\SupportedCurrency;
use App\Enum\OperationType;
use App\Exceptions\CurrencyException;
use App\Exceptions\OperationTypeException;
use App\Exceptions\ClientTypeException;
use DateTime;

class Operation implements ModelInterface
{
    private int $clientId;

    private DateTime $date;

    private string $clientType;

    private string $operationType;

    private float $operationAmount;

    private string $operationCurrency;

    private float $fee = 0;

    public function getClientId(): int
    {
        return $this->clientId;
    }

    public function setClientId(int $clientId): static
    {
        $this->clientId = $clientId;

        return $this;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getClientType(): string
    {
        return $this->clientType;
    }

    /**
     * @throws ClientTypeException
     */
    public function setClientType(string $clientType): static
    {
        if (!\in_array($clientType, (new ClientType())->getFields())) {
            throw (new ClientTypeException($clientType));
        }

        $this->clientType = $clientType;

        return $this;
    }

    public function getOperationType(): string
    {
        return $this->operationType;
    }

    /**
     * @throws OperationTypeException
     */
    public function setOperationType(string $operationType): static
    {
        if (!\in_array($operationType, (new OperationType())->getFields())) {
            throw (new OperationTypeException($operationType));
        }

        $this->operationType = $operationType;

        return $this;
    }

    public function getOperationAmount(): float
    {
        return $this->operationAmount;
    }

    public function setOperationAmount(float $operationAmount): static
    {
        $this->operationAmount = $operationAmount;

        return $this;
    }

    public function getOperationCurrency(): string
    {
        return $this->operationCurrency;
    }

    /**
     * @throws CurrencyException
     */
    public function setOperationCurrency(string $operationCurrency): static
    {
        if (!\in_array($operationCurrency, (new SupportedCurrency())->getFields())) {
            throw (new CurrencyException($operationCurrency));
        }

        $this->operationCurrency = $operationCurrency;

        return $this;
    }

    public function getFee(): float
    {
        return $this->fee;
    }

    public function setFee(float $fee): static
    {
        $this->fee = $fee;

        return $this;
    }
}
