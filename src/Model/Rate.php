<?php

declare(strict_types=1);

namespace App\Model;

use App\Enum\Currency\SupportedCurrency;
use App\Exceptions\CurrencyException;

class Rate implements ModelInterface
{
    private ?string $currency = null;

    private ?float $rate = null;

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @throws CurrencyException
     */
    public function setCurrency(?string $currency): static
    {
        if (!\in_array($currency, (new SupportedCurrency())->getFields(), true)) {
            throw (new CurrencyException($currency));
        }

        $this->currency = $currency;

        return $this;
    }

    public function getRate(): ?float
    {
        return $this->rate;
    }

    public function setRate(?float $rate): static
    {
        $this->rate = $rate;

        return $this;
    }
}
