<?php

declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class CurrencyException extends HttpException implements ValidationExceptionInterface
{
    public function __construct(string $currency)
    {
        $message = sprintf('Sorry, currency "%s" now is unsupported...', $currency);

        parent::__construct(422, $message);
    }
}