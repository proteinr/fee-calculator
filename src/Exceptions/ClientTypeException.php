<?php

declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ClientTypeException extends HttpException implements ValidationExceptionInterface
{
    public function __construct(string $clientType)
    {
        $message = sprintf('Sorry, client type "%s" now is unsupported...', $clientType);

        parent::__construct(422, $message);
    }
}