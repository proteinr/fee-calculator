<?php

declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class OperationTypeException extends HttpException implements ValidationExceptionInterface
{
    public function __construct(string $operationType)
    {
        $message = sprintf('Sorry, operation type "%s" now is unsupported...', $operationType);

        parent::__construct(422, $message);
    }
}