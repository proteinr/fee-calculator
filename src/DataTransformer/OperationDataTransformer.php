<?php

declare(strict_types=1);

namespace App\DataTransformer;

use App\Enum\Import\OperationFields;
use App\Model\ModelInterface;
use App\Model\Operation;
use DateTime;

class OperationDataTransformer implements DataTransformerInterface
{
    public function transform(array $rawObject): ModelInterface
    {
        return (new Operation())
            ->setClientId((int)$rawObject[OperationFields::CLIENT_ID])
            ->setDate((new DateTime($rawObject[OperationFields::OPERATION_DATE])))
            ->setClientType($rawObject[OperationFields::CLIENT_TYPE])
            ->setOperationType($rawObject[OperationFields::OPERATION_TYPE])
            ->setOperationAmount((float)$rawObject[OperationFields::OPERATION_AMOUNT])
            ->setOperationCurrency($rawObject[OperationFields::OPERATION_CURRENCY]);
    }
}