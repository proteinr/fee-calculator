<?php

declare(strict_types=1);

namespace App\DataTransformer;

use App\Model\ModelInterface;

interface DataTransformerInterface
{
    public function transform(array $rawObject): ModelInterface;
}