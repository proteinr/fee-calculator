<?php

declare(strict_types=1);

namespace App\DataTransformer;

use App\Enum\Currency\CurrencyFields;
use App\Model\Rate;

class CurrencyDataTransformer implements DataTransformerInterface
{
    public function transform(array $rawObject): Rate
    {
        return (new Rate())
            ->setCurrency($rawObject[CurrencyFields::CURRENCY])
            ->setRate((float)$rawObject[CurrencyFields::RATE]);
    }
}