<?php

declare(strict_types=1);

namespace App\Import;

use Ramsey\Collection\CollectionInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface ImportInterface
{
    public function import(UploadedFile $fileBag): CollectionInterface;
}