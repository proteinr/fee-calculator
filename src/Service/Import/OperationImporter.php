<?php

declare(strict_types=1);

namespace App\Import;

use App\DataTransformer\OperationDataTransformer;
use App\Enum\Import\OperationFields;
use App\Exceptions\OperationTypeException;
use App\Exceptions\ValidationExceptionInterface;
use App\Model\Operation;
use Ramsey\Collection\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class OperationImporter implements ImportInterface
{
    public function __construct(private OperationDataTransformer $dataTransformer)
    {
    }

    /**
     * @throws ValidationExceptionInterface
     */
    public function import(UploadedFile $file): Collection
    {
        $openedFile = file($file->getPathname());
        $feeCollection = new Collection(Operation::class, []);
        $keys = (new OperationFields())->getFields();

        foreach ($openedFile as $line) {
            $rawObject = array_combine($keys, str_getcsv($line));
            $feeCollection->add($this->dataTransformer->transform($rawObject));
        }

        return $feeCollection;
    }
}