<?php

declare(strict_types=1);

namespace App\Service\Exchanger;

use App\Enum\Currency\SupportedCurrency;
use App\Exceptions\CurrencyException;
use App\Model\Rate;
use Ramsey\Collection\Collection;

class Exchanger
{
    public function __construct(private Collection $rates)
    {
    }

    /**
     * @throws CurrencyException
     */
    public function convertAmountToEuro(string $currency, float $amount): float
    {
        $this->validateCurrency($currency);

        /** @var Rate $rate */
        $rate = $this->rates->where('getCurrency', $currency)->first();

        return $amount / $rate->getRate();
    }

    /**
     * @param string $currency
     * @throws CurrencyException
     */
    private function validateCurrency(string $currency): void
    {
        $supportedCurrencies = (new SupportedCurrency)->getFields();

        if (!\in_array($currency, $supportedCurrencies)) {
            throw new CurrencyException($currency);
        }
    }

    /**
     * @throws CurrencyException
     */
    public function convertAmountFromEuro(string $currency, float $amount): float
    {
        $this->validateCurrency($currency);

        /** @var Rate $rate */
        $rate = $this->rates->where('getCurrency', $currency)->first();

        return $amount * $rate->getRate();
    }

    public function roundAmount(string $currency, float $amount, int $precision = 2): float|int
    {
        return match ($currency) {
            SupportedCurrency::JPY => ceil($amount),
            default => round(ceil($amount * 100) / 100, $precision)
        };
    }
}