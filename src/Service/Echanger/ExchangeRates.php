<?php

declare(strict_types=1);

namespace App\Service\Exchanger;

use App\DataTransformer\CurrencyDataTransformer;
use App\Enum\Currency\CurrencyFields;
use App\Enum\Currency\SupportedCurrency;
use App\Model\Rate;
use GuzzleHttp\Client;
use Ramsey\Collection\Collection;

class ExchangeRates
{
    private const URL = '/v1/latest';

    private const GET_METHOD = 'GET';

    public function __construct(
        private Client $client,
        private string $apiKey,
        private CurrencyDataTransformer $dataTransformer)
    {
    }

    /**
     * @return Collection|Rate[]
     */
    public function getRates(): Collection
    {
        $endpoint = $this->decorateEndpointByApiKey($this->apiKey);
        $rawRates = $this->executeRequest($endpoint);

//        $rawRates = json_decode('{"success":true,"timestamp":1634672463,"base":"EUR","date":"2021-10-19","rates":{"EUR":1,"JPY":129.53,"USD":1.1497}}', true)['rates'];

        return $this->convertResult($rawRates);
    }

    private function decorateEndpointByApiKey(string $apiKey): string
    {
        return sprintf('%s?access_key=%s', static::URL, $apiKey);
    }

    private function convertResult($data): Collection
    {
        $rates = new Collection(Rate::class, []);
        $availableCurrencies = (new SupportedCurrency())->getFields();

        foreach ($availableCurrencies as $currency) {
            $rates[] = $this->dataTransformer->transform([
                CurrencyFields::CURRENCY => $currency,
                CurrencyFields::RATE => $data[$currency]
            ]);
        }

        return $rates;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function executeRequest(string $endpoint): array
    {
        $response = $this->client->request(static::GET_METHOD, $endpoint);

        return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR)['rates'];
    }
}