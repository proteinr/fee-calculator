<?php

declare(strict_types=1);

namespace App\Controller\v1;

use App\Manager\v1\FeeManger;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FeeController extends AbstractController
{
    public function __construct(private FeeManger $manager)
    {
    }

    /**
     * @Route("/v1/calculate", methods={"POST"})
     */
    public function calculate(Request $request): JsonResponse
    {
        $file = $request->files->getIterator()->current();

        return $this->json(
            $this->manager->calculate($file),
            context: ['json_encode_options' => JSON_PRESERVE_ZERO_FRACTION]
        );
    }
}