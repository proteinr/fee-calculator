<?php

declare(strict_types=1);

namespace App\Manager\v1;

use App\Decorator\Fee\FeeDecoratorInterface;
use App\Decorator\Fee\Deposit\FeeDepositDecorator;
use App\Decorator\Fee\Withdraw\FeeWithdrawDecorator;
use App\Enum\OperationType;
use App\Exceptions\ValidationExceptionInterface;
use App\Service\Exchanger\Exchanger;
use App\Service\Exchanger\ExchangeRates;
use App\Import\OperationImporter;
use App\Model\Operation;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use function PHPUnit\Framework\throwException;

class FeeManger
{
    public function __construct(
        private OperationImporter $importer,
        private ExchangeRates $ratesService
    ) {
    }

    public function calculate(UploadedFile $file): array
    {
        $operations = $this->importer->import($file);
        $rates = $this->ratesService->getRates();
        $exchanger = new Exchanger($rates);

        /** @var Operation $operation */
        foreach ($operations as $operation) {
            $decorator = $this->getFeeDecorator($operation->getOperationType());

            /** @var FeeDecoratorInterface $decorator */
            (new $decorator($operation, $exchanger, $operations))->decorate();
        }

        return $operations->column('getFee');
    }

    /**
     * @param string $operationType
     * @return string|FeeDecoratorInterface
     */
    private function getFeeDecorator(string $operationType): string
    {
        $decorators = [
            OperationType::WITHDRAW => FeeWithdrawDecorator::class,
            OperationType::DEPOSIT => FeeDepositDecorator::class,
        ];

        return $decorators[$operationType] ?? FeeDepositDecorator::class;
    }
}