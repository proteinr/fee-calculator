<?php

declare(strict_types=1);

namespace App\Enum\Import;

class OperationFields
{
    public const OPERATION_DATE = 'operation_date';

    public const CLIENT_ID = 'client_id';

    public const CLIENT_TYPE = 'client_type';

    public const OPERATION_TYPE = 'operation_type';

    public const OPERATION_AMOUNT = 'operation_amount';

    public const OPERATION_CURRENCY = 'operation_currency';

    public function getFields(): array
    {
        return [
            self::OPERATION_DATE,
            self::CLIENT_ID,
            self::CLIENT_TYPE,
            self::OPERATION_TYPE,
            self::OPERATION_AMOUNT,
            self::OPERATION_CURRENCY,
        ];
    }
}