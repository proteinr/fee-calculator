<?php

declare(strict_types=1);

namespace App\Enum\Currency;

class SupportedCurrency
{
    public const EUR = 'EUR';

    public const USD = 'USD';

    public const JPY = 'JPY';

    public function getFields(): array
    {
        return [
            self::EUR,
            self::USD,
            self::JPY,
        ];
    }
}