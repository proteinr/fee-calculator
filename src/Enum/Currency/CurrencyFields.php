<?php

declare(strict_types=1);

namespace App\Enum\Currency;

class CurrencyFields
{
    public const CURRENCY = 'currency';

    public const RATE = 'rate';

    public function getFields(): array
    {
        return [
            self::CURRENCY,
            self::RATE,
        ];
    }
}