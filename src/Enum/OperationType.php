<?php

declare(strict_types=1);

namespace App\Enum;

class OperationType
{
    public const WITHDRAW = 'withdraw';

    public const DEPOSIT = 'deposit';

    public function getFields(): array
    {
        return [
            self::WITHDRAW,
            self::DEPOSIT,
        ];
    }
}