<?php

declare(strict_types=1);

namespace App\Enum;

class CommissionFee
{
    public const DEPOSIT_COMMISSION = 0.03;

    public const WITHDRAW_BUSINESS_COMMISSION = 0.5;

    public const WITHDRAW_PRIVATE_COMMISSION = 0.3;

    public const WITHDRAW_PRIVATE_AMOUNT_WITHOUT_COMMISSION = 1000;

    public const WITHDRAW_PRIVATE_OPERATIONS_WITHOUT_COMMISSION = 3;

    public function getFields(): array
    {
        return [
            self::DEPOSIT_COMMISSION,
            self::WITHDRAW_BUSINESS_COMMISSION,
            self::WITHDRAW_PRIVATE_COMMISSION,
            self::WITHDRAW_PRIVATE_AMOUNT_WITHOUT_COMMISSION,
            self::WITHDRAW_PRIVATE_OPERATIONS_WITHOUT_COMMISSION,
        ];
    }
}