<?php

declare(strict_types=1);

namespace App\Enum;

class ClientType
{
    public const PRIVATE = 'private';

    public const BUSINESS = 'business';

    public function getFields(): array
    {
        return [
            self::PRIVATE,
            self::BUSINESS,
        ];
    }
}