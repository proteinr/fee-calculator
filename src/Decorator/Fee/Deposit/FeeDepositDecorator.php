<?php

declare(strict_types=1);

namespace App\Decorator\Fee\Deposit;

use App\Decorator\Fee\FeeAbstractDecorator;
use App\Enum\CommissionFee;
use App\Model\Operation;

class FeeDepositDecorator extends FeeAbstractDecorator
{
    public function decorate(): Operation
    {
        $rawFee = $this->calculateFee();
        $fee = $this->exchanger->roundAmount($this->operation->getOperationCurrency(), $rawFee);

        $this->operation->setFee($fee);

        return parent::decorate();
    }

    private function calculateFee(): float
    {
        $operationCurrency = $this->operation->getOperationCurrency();
        $operationAmount = $this->operation->getOperationAmount();

        $amountInEur = $this->exchanger->convertAmountToEuro($operationCurrency, $operationAmount);
        $feeInEur = $this->processCalculation($amountInEur);

        return $this->exchanger->convertAmountFromEuro($operationCurrency, $feeInEur);
    }

    private function processCalculation(float $amountInEur): float
    {
        return $amountInEur / 100 * CommissionFee::DEPOSIT_COMMISSION;
    }
}