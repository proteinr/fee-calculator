<?php

declare(strict_types=1);

namespace App\Decorator\Fee\Withdraw;

use App\Decorator\Fee\FeeAbstractDecorator;
use App\Decorator\Fee\Withdraw\FeeCalculator\BusinessClientFeeCalculator;
use App\Decorator\Fee\Withdraw\FeeCalculator\PrivateClientFeeCalculator;
use App\Enum\ClientType;
use App\Model\Operation;

class FeeWithdrawDecorator extends FeeAbstractDecorator
{
    public function decorate(): Operation
    {
        $rawFee = $this->calculateFee();
        $fee = $this->exchanger->roundAmount($this->operation->getOperationCurrency(), $rawFee);

        $this->operation->setFee($fee);

        return parent::decorate();
    }

    private function calculateFee(): float
    {
        $operationCurrency = $this->operation->getOperationCurrency();
        $operationAmount = $this->operation->getOperationAmount();

        $amountInEur = $this->exchanger->convertAmountToEuro($operationCurrency, $operationAmount);
        $feeInEur = $this->processCalculation($amountInEur, $this->operation);

        return $this->exchanger->convertAmountFromEuro($operationCurrency, $feeInEur);
    }

    private function processCalculation(float $amountInEur, Operation $operation): float
    {
        $calculator = $this->getCalculator($operation->getClientType());

        return (new $calculator($this->operations, $this->exchanger))->calculate($amountInEur, $operation);
    }

    private function getCalculator(string $clientType): string
    {
        $calculators = [
            ClientType::BUSINESS => BusinessClientFeeCalculator::class,
            ClientType::PRIVATE => PrivateClientFeeCalculator::class,
        ];

        return $calculators[$clientType] ?? $calculators[ClientType::BUSINESS];
    }
}