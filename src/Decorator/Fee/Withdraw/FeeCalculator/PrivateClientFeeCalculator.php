<?php

declare(strict_types=1);

namespace App\Decorator\Fee\Withdraw\FeeCalculator;

use App\Enum\CommissionFee;
use App\Enum\OperationType;
use App\Model\Operation;
use App\Service\Exchanger\Exchanger;
use DateTime;
use Ramsey\Collection\Collection;

class PrivateClientFeeCalculator implements FeeCalculatorInterface
{
    public function __construct(private Collection $allOperations, private Exchanger $exchanger)
    {
    }

    public function calculate(float $amountInEur, Operation $operation): float
    {
        $operationsGroupedByWeek = $this->groupClientOperationsByWeek($this->allOperations, $operation);
        $countOperationsInWeek = $this->calculateCountOperationsInWeek($operation, $operationsGroupedByWeek);
        $amountOfOperationsInWeek = $this->calculateAmountOperationsInWeek($operation, $operationsGroupedByWeek);

        if ($this->isCountFreeOperationsExceeded($countOperationsInWeek)) {
            return $amountInEur / 100 * CommissionFee::WITHDRAW_PRIVATE_COMMISSION;
        }

        if ($this->isOperationsAmountExceeded($amountOfOperationsInWeek)) {
            return 0;
        }

        $amountInEur = $this->getAmountForFee($countOperationsInWeek, $amountInEur, $amountOfOperationsInWeek);

        return $amountInEur / 100 * CommissionFee::WITHDRAW_PRIVATE_COMMISSION;
    }

    private function groupClientOperationsByWeek(Collection $operations, Operation $currentOperation): array
    {
        $clientOperations = $operations->where('getClientId', $currentOperation->getClientId());

        $clientOperationsByWeek = [];
        /** @var Operation $clientOperation */
        foreach ($clientOperations as $clientOperation) {
            $key = $this->generateArrayKey($clientOperation->getDate()->getTimestamp());

            if ($clientOperation->getOperationType() === OperationType::WITHDRAW) {
                $clientOperationsByWeek[$key][] = $clientOperation;
            }
        }

        return $clientOperationsByWeek;
    }

    private function generateArrayKey(int $timeStamp): string
    {
        $weekDay = (new DateTime())->setTimestamp($timeStamp)->format('D');

        $timestampOfMonday = $weekDay === 'Mon' ? $timeStamp : strtotime('last Monday', $timeStamp);
        $mondayOfWeek = (new DateTime())->setTimestamp($timestampOfMonday)->format('Y-m-d');

        $timestampOfSunday = $weekDay === 'Sun' ? $timeStamp : strtotime('next Sunday', $timeStamp);
        $sundayOfWeek = (new DateTime())->setTimestamp($timestampOfSunday)->format('Y-m-d');

        return sprintf('%s - %s', $mondayOfWeek, $sundayOfWeek);
    }

    private function calculateCountOperationsInWeek(Operation $currentOperation, array $operationsGroupedByWeek): int
    {
        foreach ($operationsGroupedByWeek as $operationsInWeek) {
            $key = $this->getOperationNumberInWeek($operationsInWeek, $currentOperation);

            if ($key) {
                return $key;
            }
        }

        return 1;
    }

    private function getOperationNumberInWeek(array $operationsInWeek, Operation $currentOperation): ?int
    {
        foreach ($operationsInWeek as $key => $operation) {
            if ($operation === $currentOperation) {
                return ++$key;
            }
        }

        return null;
    }

    private function calculateAmountOperationsInWeek(Operation $currentOperation, array $operationsGroupedByWeek): float
    {
        $key = $this->generateArrayKey($currentOperation->getDate()->getTimestamp());

        $operationsInWeek = $operationsGroupedByWeek[$key];

        $amountInEur = 0;
        /** @var Operation $operation */
        foreach ($operationsInWeek as $operation) {
            $operationAmountInEur = $this->exchanger->convertAmountToEuro(
                $operation->getOperationCurrency(),
                $operation->getOperationAmount()
            );
            $amountInEur += $operationAmountInEur;

            if ($currentOperation === $operation) {
                return $amountInEur;
            }
        }

        return $amountInEur;
    }

    private function isCountFreeOperationsExceeded(int $countOperationsInWeek): bool
    {
        return $countOperationsInWeek > CommissionFee::WITHDRAW_PRIVATE_OPERATIONS_WITHOUT_COMMISSION;
    }

    private function isOperationsAmountExceeded(float $amountOfOperationsInWeek): bool
    {
        return $amountOfOperationsInWeek < CommissionFee::WITHDRAW_PRIVATE_AMOUNT_WITHOUT_COMMISSION;
    }

    private function getAmountForFee(int $countOperationsInWeek, float $amountInEur, float $amountOfOperationsInWeek): float
    {
        if ($countOperationsInWeek === 1) {
            return $amountInEur - CommissionFee::WITHDRAW_PRIVATE_AMOUNT_WITHOUT_COMMISSION;
        }

        $previousPaid = abs($amountInEur - $amountOfOperationsInWeek);

        return (float)$previousPaid < CommissionFee::WITHDRAW_PRIVATE_AMOUNT_WITHOUT_COMMISSION ? $previousPaid : $amountInEur;
    }
}