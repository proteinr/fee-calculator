<?php

declare(strict_types=1);

namespace App\Decorator\Fee\Withdraw\FeeCalculator;

use App\Service\Exchanger\Exchanger;
use App\Model\Operation;
use Ramsey\Collection\Collection;

interface FeeCalculatorInterface
{
    public function __construct(Collection $allOperations, Exchanger $exchanger);
    
    public function calculate(float $amountInEur, Operation $operation): float;
}