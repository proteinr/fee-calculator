<?php

declare(strict_types=1);

namespace App\Decorator\Fee;

use App\Service\Exchanger\Exchanger;
use App\Model\Operation;
use Ramsey\Collection\Collection;

interface FeeDecoratorInterface
{
    /**
     * @param Operation $operation
     * @param Exchanger $exchanger
     * @param Collection $operations
     */
    public function __construct(Operation $operation, Exchanger $exchanger, Collection $operations);

    public function decorate(): Operation;
}