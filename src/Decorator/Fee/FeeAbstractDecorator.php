<?php

declare(strict_types=1);

namespace App\Decorator\Fee;

use App\Service\Exchanger\Exchanger;
use App\Model\Operation;
use Ramsey\Collection\Collection;

class FeeAbstractDecorator implements FeeDecoratorInterface
{
    public function __construct(
        protected Operation $operation,
        protected Exchanger $exchanger,
        protected Collection $operations) {
    }

    public function decorate(): Operation
    {
        return $this->operation;
    }
}