<?php

declare(strict_types=1);

namespace App\tests\Functional;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Ramsey\Collection\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FeeControllerTest extends AbstractTest
{
    private Client $httpClient;

    protected function setUp(): void
    {
        parent::setUp();
        $this->httpClient = $this->createMock(Client::class);
    }

    /**
     * @dataProvider provideExpectedCalculations
     */
    public function testCalculateWillReturnExpectedFeeCalculations($rates, $expectedCalculations): void
    {
        $container = self::$container;
        $container->set('httpExchangerClient', $this->httpClient);
        $this->httpClient
            ->expects($this->once())
            ->method('request')
            ->willReturn($rates);

        $file = new UploadedFile(realpath('./tests/Files/input.csv'), 'input');
        $this->client->request('POST', '/v1/calculate', [], ['file' => $file]);
        $response = $this->client->getResponse();
        $calculatedData = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertEquals($expectedCalculations, $calculatedData);
    }

    public function provideExpectedCalculations(): array
    {
        $ratesJson = (new Response(body: '{"success":true,"timestamp":1634672463,"base":"EUR","date":"2021-10-19","rates":{"EUR":1,"JPY":129.53,"USD":1.1497}}'));

        return [
            [
                'rates' => $ratesJson,
                'expectedCalculations' => [
                    0.60,
                    3.00,
                    0.00,
                    0.06,
                    1.50,
                    0.0,
                    0.70,
                    0.30,
                    0.30,
                    3.00,
                    0.00,
                    0.00,
                    8612,
                ]
            ],
        ];
    }
}